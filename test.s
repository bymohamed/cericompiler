			# This code was produced by the CERI Compiler
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
vrai:
	push $1
	jmp fin
faux:
	push $0
	jmp fin
fin:
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
